package com.mycompany.crudexample.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mycompany.crudexample.entity.Product;
import com.mycompany.crudexample.repository.ProductRepository;

@Service
public class ProductService {
	
	@Autowired
	ProductRepository productRepo;
	
	public Product saveProduct(Product product) {
		return productRepo.save(product);
	}
	
	public List<Product> saveProducts(List<Product> product) {
		return productRepo.saveAll(product);
	}
	
	public List<Product> getProduct(){
		return productRepo.findAll();
	}
	
	public Product getProductById(int id) {
		return productRepo.findById(id).orElse(null);
	}
	
	public Product getProductByName(String name) {
		return productRepo.findByName(name).orElse(null);
	}
	
	public String deleteProductById(int id) {
		productRepo.deleteById(id);
		return "product removed !! "+id;
	}
	
	public Product updateProduct(Product product) {
		Product existingProduct = productRepo.findById(product.getId()).orElse(product);
		existingProduct.setName(product.getName());
		existingProduct.setPrice(product.getPrice());
		existingProduct.setQuantity(product.getQuantity());
		productRepo.save(existingProduct);
		return existingProduct;
		
	}

}
