package com.mycompany.crudexample.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.crudexample.entity.Product;
import com.mycompany.crudexample.service.ProductService;

@RestController
@RequestMapping(path = "/v1")
public class ProductController {

	@Autowired
	private ProductService service;
	
	@PostMapping("/product")
	public Product addProduct(@RequestBody Product product) {
		return service.saveProduct(product);
	}
	
	@PostMapping("/products")
	public List<Product> addProducts(@RequestBody List<Product> product){
		return service.saveProducts(product);
	} 
	
	@GetMapping("/product/{id}")
	public Product getProduct(@PathVariable int id) {
		return service.getProductById(id);
	}
	
	@GetMapping("/product/name/{name}")
	public Product getProduct(@PathVariable String name) {
		return service.getProductByName(name);
	}
	
	@GetMapping("/products")
	public List<Product> getAllProducts(){
		return service.getProduct();
	} 
	
	@PutMapping("/product")
	public Product updateProduct(@RequestBody Product product) {
		return service.updateProduct(product);
		
	}
	
	@DeleteMapping("/product/{id}")
	public String deleteProduct(@PathVariable int id) {
		return service.deleteProductById(id);
	}
	
}
